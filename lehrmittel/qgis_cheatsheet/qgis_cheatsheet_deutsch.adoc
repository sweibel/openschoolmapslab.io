= QGIS Cheatsheet
:author: {blank}
:experimental:
:imagesdir: ../../bilder/qgis_cheatsheet/
include::../../snippets/lang/de.adoc[]
include::../../snippets/suppress_title_page.adoc[]

== GUI Beschreibung
image::qgis_beschreibung_screenshot.png[QGIS Description]

Es gibt weitere Bedienflächen oder Werkzeugkästen, die nicht sichtbar sind. Um diese zu öffnen kann man einfach auf "Ansicht > Bedienfelder" oder "Ansicht > Werkzeugkästen"

Ein weiterer Weg dazu ist auf die graue Fläche bei den Werkzeugkästen rechts zu klicken. Dann sollte ein Fenster aufgehen, bei dem man sowohl Bedienfelder, als auch Werkzeugkästen öffnen kann.

<<<

=== Statusleiste
image::qgis_statusleiste.png[QGIS Statusleiste]

Von links nach rechts:

. Koordinate: Zeigt die Koordinaten an, bei denen sich der Mauszeiger im Karten Fenster befindet. Die Koordinaten sind abhängig von dem gewählten Koordinatenbezugssystem (KBS).
. Massstab: Zeigt den Massstab an auf welchem die Karte sichtbar ist im Karten Fenster. Den Massstab kann mit hinein- und herauszoomen oder indem man eine der vordefinierten Optionen aus dem Dropdown Menü auswählt.
. Vergrösserung: Wenn man auf das image:qgis_lock.png[Schloss] Schloss klickt, kann man hinein- und herauszoomen ohne den Masstab zu verändern. Der Standardwert ist 100%.
. Drehung: Mit dieser Option kann man die Karte im Uhrzeigersinn drehen.
. Zeichnen: Kann genutzt werden, um das zeichnen der Karte temporär auszuschalten.
. Wenn man auf das image:qgis_crs.png[KBS Icon] KBS Icon klickt, dann kann man das Koordinatenbezugssystem ändern.
. Wenn man auf die image:qgis_bubble.png[Sprechblase] Sprechblase klickt, dann öffnet sich das Protokoll Fenster.

image::qgis_protokoll.png[Log Messages]

== Menü und Werkzeugkästen

=== Navigation im Karten Fenster

.Navigation im Karten Fenster
[cols="3,1,4,3"]
|===
| Name | Icon | Tastenkürzel | Beschreibung

|Karte verschieben
a| image::qgis_pan_map.png[Verschieben, 30, 30]
a| kbd:[Leertaste], kbd:[Page Up], kbd:[Page Down] 

oder mit den Pfeiltasten
| Verschiebe die Karte

|Karte zu gewählten Objekt verschieben
a| image::qgis_pan_map_selection.png[Karte zu gewählten Objekt verschieben]
| 
| Karte zu dem gewählten Objekt verschieben

|Hineinzoomen
a| image::qgis_zoom_in.png[Hineinzoomen, 30, 30]
| kbd:[Strg + Alt + +] oder Mausrad
| In die Karte hineinzoomen

|Herauszoomen
a| image::qgis_zoom_out.png[Herauszoomen, 30, 30]
| kbd:[Strg + Alt + -] oder Mausrad
| In die Karte herauszoomen

|Volle Ausdehnung
a| image::qgis_zoom_full.png[Volle Ausdehnung, 30, 30]
| kbd:[Strg + Umschalt + F]
| Herauszoomen, um die komplette Karte zu zeigen

|Zu ausgewählten Objekt zoomen
a| image::qgis_zoom_to_selection.png[Zu ausgewählten Objekt zoomen, 30, 30]
| kbd:[Strg + J]
| Auf der Karte zu dem ausgewählten Objekt zoomen

|Auf Layer zoomen
a| image::qgis_zoom_to_layer.png[Auf Layer zoomen, 30, 30]
|
|Auf den ausgewählten Layer zoomen

|Auf eigene Auflösung zoomen
a| image::qgis_zoom_native_resolution.png[Auf eigene Auflösung zoomen, 30, 30]
|
|Auf die eigene Auflösung zoomen (100%)

|Zoom zurück
a| image::qgis_zoom_last.png[Zoom zurück, 30, 30]
|
| Zum letzten Zoomwert zurück springen

|Zoom vor
a| image::qgis_zoom_next.png[Zoom vor, 30, 30]
|
|Zum nächsten Zoomwert springen

|===

=== Projektmanagement

.Projektmanagement
[cols="3,1,4,3"]
|===
| Name | Icon | Tastenkürzel | Beschreibung

| Neues Projekt
a| image::qgis_new.png[Neues Projekt, 30, 30]
| kbd:[Strg + N]
| Erstelle ein neues Projekt

| Projekt öffnen
a| image::qgis_open_project.png[Projekt öffnen, 30, 30]
| kbd:[Strg + O]
| Öffne ein bereits existierendes Projekt

| Speichern
a| image::qgis_save_project.png[Save, 30, 30]
| kbd:[Strg + S]
| Speichere das Projekt

| Speichern als...
a| image::qgis_save_project_as.png[Speichern als..., 30, 30]
| kbd:[Strg + Umschalt + S]
| Speichere das Projekt als...

| Eigenschaften
a|
| kbd:[Strg + Umschalt + P]
| Öffnet die Projekt Eigenschaften

| Neues Drucklayout
a| image::qgis_new_print_layerout.png[Neues Drucklayout, 30, 30]
| kbd:[Strg + P]
| Öffnet den Dialog, um ein neues Drucklayout zu erstellen.

| Suche
a|
| kbd:[Strg + K]
| Öffnet die Suchleiste

|===

=== Layermanagement

.Layermanagement
[cols="3,1,4,3"]
|===
| Name | Icon | Tastenkürzel | Beschreibung

|Datenquellenverwaltung
a| image::qgis_data_source_manager.png[Datenquellenverwaltung, 30, 30]
| kbd:[Strg + L]
| Einen neuen Layer hinzufügen

| Neuer GeoPackage-Layer
a| image::qgis_new_geopackage_layer.png[Neuer GeoPackage-Layer, 30, 30]
| kbd:[Strg + Umschalt + N]
| Einen neuen GeoPackage-Layer hinzufügen

| Vektorlayer hinzufügen
a| image::qgis_add_vector_layer.png[Add vector layer, 30, 30]
| kbd:[Strg + Umschalt + V]
| Einen neuen Vektorlayer hinzufügen

| Rasterlayer hinzufügen.
a| image::qgis_add_raster_layer.png[Add vector layer, 30, 30]
| kbd:[Strg + Umschalt + R]
| Einen neuen Rasterlayer hinzufügen

| Layer/Gruppe löschen
a| image::qgis_remove_selected_layer.png[Layer/Gruppe löschen, 30, 30]
| kbd:[Strg + D]
| Den ausgewählten Layer/die ausgewählte Gruppe löschen

| Layer Fenster ein/ausblenden
a| 
| kbd:[Strg + 1]
| Das Layer Bedienfeld ein und ausblenden

| Browser Fenster ein/ausblenden
a|
| kbd:[Strg + 2]
| Das Browser Bedienfeld ein und ausblenden
|===

.Analyse Tools
[cols="3,1,4,3"]
|===
| Name | Icon | Tastenkürzel | Beschreibung

| Objekte abfragen
a| image::qgis_identify_features.png[Objekte abfragen, 30, 30]
| kbd:[Strg + Umschalt + I]
| Objekte in dem Karten Fesnter abfragen, indem man auf diese klickt

| Objekt auswählen
a| image::qgis_select_features.png[Objekt auswählen, 30, 30]
|
| Ein Objekt über Rechteck oder Einzelklick wählen

| Objekt über Wert wählen
a| image::qgis_select_features_by_value.png[Objekt über Wert wählen, 30, 30]
| kbd:[F3]
| Objekt über einen Wert wählen

| Attributtabelle öffnen
a| image::qgis_open_attribute_table.png[Attributtabelle öffnen, 30, 30]
| kbd:[F6]
| Öffnet die Attributtabelle

| Attributtabelle öffnen mit ausgewählten Objekten
a| image::qgis_open_attribute_table.png[Attributtabelle öffnen mit ausgewählten Objekten, 30, 30]
| kbd:[Umschalt + F6]
| Öffnet die Attributtabelle mit nur den ausgewählten Objekten

| Attributtabelle öffnet mit sichtbaren Objekten
a| image::qgis_open_attribute_table.png[Attributtabelle öffnet mit sichtbaren Objekten, 30, 30]
| kbd:[Strg + F6]
| Öffnet die Attributtabelle mit nur den sichtbaren Objekten

|===

<<<

=== Fortgeschrittene Werkzeuge

.Fortgeschrittene Werkzeuge
[cols="3,1,2,3"]
|===
| Name | Icon | Tastenkürzel | Beschreibung

| Verarbeitungswerkzeuge
a| image::qgis_processing_toolbox.png[Verarbeitungswerkzeuge, 30, 30]
| kbd:[Strg + Alt + T]
| Öffnet die Verarbeitungswerkzeugkiste

| Python-Konsole
a| image::qgis_python_console.png[Python-Konsole, 30, 30]
| kbd:[Strg + Alt + P]
| Öffnet die Python-Konsole

|===

Für mehr Tastenkürzel oder Informationen kann man die https://www.qgis.org/de/docs/index.html[QGIS Dokumentation] lesen.